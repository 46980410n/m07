![IIS-logo](/images/iis-logo.png)

En esta práctica activarás el servicio IIS en un equipo con Windows 10 instalado, puede ser una máquina virtual o una máquia real, a tu elección.

[Aquí](https://dungeonofbits.com/activacion-de-internet-information-services-iis.html) tienes un tutorial para activar IIS en el equipo.

1.- Activa IIS en el equipo y documenta el proceso.

![Alt](/images/iis0.png)
Abaix a l'esquerra, busquem "panel de control"

![Alt](/images/iis1.png)
E "Panel de control" fem click a "Programas y características".

![Alt](/images/iis2.png)
A l'esquerra, clickem a "Activar o desactivar características de Windows"

![Alt](/images/iis3.png)
Aquí simplement ens cal activar "Internet Information Services" o IIS

2.- Crea un grupo de aplicaciones que se llame "APELLIDO_pool", donde APELLIDO es tu apellido.
![Alt](/images/iis4.png)


3.- Cambia el nombre de la página por defecto que viene en IIS por "Smixers_APELLIDO", donde APELLIDO es tu apellido.
![Alt](/images/iis5.png)


4.- Haz que la página "Smixers_APELLIDO" pertenezca al Application pool "APELLIDO_pool".
![Alt](/images/iis6.png)


5.- ¿Si se colgase por algún error una página del "Application pool 1" se podría seguir accediendo a una página de otro Application pool?
Si, porque un pool es independiente del otro.


6.- Limita el acceso al sitio "Smixers_APELLIDO" a 10 conexiones simultáneas y documenta el proceso.
A la secció de la dreta del tot, clickem a "LIMITES"
![Alt](/images/iis7.png)
i aquí modifiquem l'apartat de "limit de connexions" a 10.


7.- ¿El examen de directorios está habilitado por defecto en IIS?
No.

8.- ¿Qué indica el examen de directorios?
Hora, Tamaño, Extension, Fecha, Fecha larga (pero todo en gris, porque está desactivado)

9.- ¿Cual es el directorio por defecto para el sitio de IIS?
C:\inetpub\wwwroot

10.- Agrega un fichero html llamado "APELLIDO.html" al directorio por defecto de IIS, este documento contendrá tu CV (El que ya hiciste en M08) o uno nuevo en formato HTML. Si lo tienes en formato md puedes utilizar un conversor md a html online.
![Alt](/images/iis8.png)


11.- Cambia las prioridades en "Documento predeterminado" del sitio para que cuando accedemos a "localhost" por el navegador muestre el curriculum del punto anterior en lugar de la página por defecto de IIS.
![Alt](/images/iis9.png)


12.- Agrega un directorio virtual llamado Poblenou que se aloje en una carpeta llamada poblenou dentro del directorio por defecto de IIS.
![Alt](/images/iis10.png)


13.- Crea una página html en la carpeta anterior que se llame index.html y contenga, al menos, tu apellido y la palabra Poblenou en ella.
![Alt](/images/iis11.png)


14.- ¿Qué debes escribir en tu navegador para mostrar el fichero creado en el punto anterior?
file:///C:/inetpub/wwwroot/poblenou/index.html


15.- Crea una página de error 404 personalizada con texto e imágenes para tu sitio (puedes basarte en las de google u otro sitio web).
![Alt](/images/iis12.png)


16.- Edita la configuración de tu sitio para que cuando se intente acceder a una página que no exista muestre la página que has creado en el punto anterior y documenta el proceso.
![Alt](/images/iis13.png)


17.- Muestra el fichero log de tu sitio "Smixers_APELLIDO".
![Alt](/images/iis14.png)

