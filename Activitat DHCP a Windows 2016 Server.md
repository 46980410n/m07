# Activitat DHCP a Windows 2016 Server


### Segueix aquests passos i documenta:

### A una instal·lació nova de Windows 2016 Server:



##### 1.- Instal·la el Servidor DHCP.


![Alt]( images/server01.png "Source Code HTML")
Aquesta serà el que veurem de bones a primeres en el nostre Servidor.

![Alt]( images/server02.png "Source Code HTML")
Fent click a "Agregar roles y características", ens surt una finestra emergent, amb diversos passos a seguir, els quals anem acceptant fins...

![Alt]( images/server03.png "Source Code HTML")
...aquesta opció, em de triar la 1ª, ja que el nostre objectiu és instal·lar un **servidor**.

![Alt]( images/server04.png "Source Code HTML")
A la següent selecció ens apareixen múltiples opcions. Busquem la opció "Servidor DHCP" i la seleccionem, acceptant les subsegüents opcions.

![Alt]( images/server05.png "Source Code HTML")
I finalment, arribem a la confirmació!

![Alt]( images/server06.png "Source Code HTML")
Per estar-ne segurs/es de que l'hem activat correctament, anem a "Herramientas" i busquem la opció "DHCP", ja que en són les opcions de gestió.


##### 2.- Crea un àmbit amb les adreces IP següents: 192.168.X.1-192.168.X.254 (on X és el teu numero d'ordinador. En el meu cas, el **12** )

![Alt]( images/ambit01.png "Source Code HTML")
Un cop som dins les opcións de gestió del servidor DHCP (recordem: Herramientas->DHCP), se'ns obra la opció de crear l'àmbit desitjat.
Simplement cal anar al desplegable de l'esquerra, botó dret en "IP" i seleccionem "Ámbito nuevo".

![Alt]( images/ambit02.png "Source Code HTML")
Acte seguit indiquem quina franja d'IPs volem que el nostre servidor ofereixi.


##### 3.- Crea una exclusió de IPs: 192.168.X.1-192.168.X.10

![Alt]( images/ambit03.png "Source Code HTML")
També ens ofereix l'opció de bloquejar/bannejar/excluir certes IP. Des d'una franja...


##### 4.- Crea una altra exclusió: 192.168.X.254-192.168.X.254

![Alt]( images/ambit03.png "Source Code HTML")
...a una específica. Tan fàcilment com repetir el "cap" i la "cua" de la franja amb la mateixa IP


##### 5.- La IP del gateway serà: 192.168.X.1

![Alt]( images/ambit05.png "Source Code HTML")
Aquí establim que la nostra IP de "sortida" o Gateway, serà el propi ordinador on instalem el Servidor.


##### 6.- La IP del DNS serà: X.X.X.X

![Alt]( images/ambit06.png "Source Code HTML")
Aquí s'estableix l'adreça del DNS. Clickem en "Agregar". És possible que ens trigui una mica (no instantani, com els casos anteriors).


##### 7.- Crea una reserva de IP per a un ordinador client (busca la seva MAC) i li donaràs l'adreça 192.168.X.X

![Alt]( images/ambit07.png "Source Code HTML")
Per crear una/es reserves d'IP, simplement hem de tornar al desplegable: doble click a "IPv4", despleguem l'àmbit creat anteriorment i ara el click dret el farem a "Reservas".

![Alt]( images/ambit08.png "Source Code HTML")
Aquí simplement hem de indicar el nom de la reserva (sí, com un restaurant de les pel·lícules), la direcció IP a reservar i la MAC de l'equip que l'utilitzarà.

![Alt]( images/ambit09.png "Source Code HTML")
(MAC que podrem trobar amb la comanda "ipconfig /all" al terminal de l'ordinador client; tal i com es mostra en aquesta imatge).


##### 8.- Elimina l'àmbit.

![Alt]( images/ambit11.png "Source Code HTML")
Per eliminar l'àmbit n'hi ha prou amb botó dret en el propi ambit i sel·leccionar "Eliminar".


##### 9.- Desactiva el servidor DHCP. (DHCP->Administrar->Quitar roles y funciones).

![Alt]( images/antiserver01.png "Source Code HTML")
Per eliminar el servidor DHCP, fem just això: "DHCP" -> "Administrar" -> "Quitar roles y funciones".

![Alt]( images/antiserver02.png "Source Code HTML")
Seleccionem el servidor DHCP que volem desactivar. És força semblant a l'anterior activació.

![Alt]( images/antiserver03.png "Source Code HTML")
Desmarquem l'opció de "Servidor DHCP".

![Alt]( images/antiserver04.png "Source Code HTML")
Clickem en "Quitar características" en la finestra que ens apareixerà al desmarcar l'opció que hem mencionat en la imatge anterior.

![Alt]( images/antiserver05.png "Source Code HTML")
Seguim acceptant i arribem a la confirmació. Altre cop ens dóna la opció de reiniciar, la qual recomanem.

