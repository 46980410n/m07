# Pràctica DNS Windows Server

Has de ficar una captura dels punts on hi hagi un **(*)**

## Prerequisits:

En aquesta primera pràctica necessitaràs els següents elements:

* Màquina virtual Windows 2016 server amb la funció de DNS instal·lada (que farà de servidor DNS).
* Màquina virtual Windows 7, 8 o 10 a escollir per tu (que farà de client). 

Les MVs estaran en **xarxa NAT**.

La IP del servidor serà 10.0.2.X on X serà l'últim byte de la IP del teu ordinador real a classe. Per exemple, si el teu ordinador a classe té la IP 172.31.84.44, el servidor tindrà la IP 10.0.2.44 **fixa**.

La IP del servidor serà 10.0.2.Y on Y serà l'últim byte de la IP del teu ordinador real a classe +100. Per exemple, si el teu ordinador a classe té la IP 172.31.84.44, el client tindrà la IP 10.0.2.144 **fixa**.

Comprova que les MVs poden fer ping entre elles abans de començar la pràctica.

## 1.- Configuració de zona:

Configuraràs una zona al servidor DNS, però abans ves al servidor DNS: *Administración del servidor->Herramientas->DNS* y a *Propiedades* esborra TOTES les *sugerencias de raíz*.

(*)Pots comprobar que el servidor no tradueix cap nom a IP fent un nslookup domini al terminal del servidor. Abans esborra la caché amb **ipconfig /flushdns**. 

![Alt]( images/dns01.png "Title")


Crearàs una *zona de búsqueda directa* que es digui **cognom.com** substituint cognom pel teu cognom real.

## 2.- Configuració de hosts amb IP privada:

Dins de la zona creada faràs un nou *Host (A)* amb el nom de Host *servidor* i la IP de la MV que fa de servidor.

(*) Mostra com les has creat.

![Alt]( images/dns02.png "Title")


(*) Comprova que pots fer ping al nom del host *servidor.cognom.com* i que la traducció de nom a IP funciona amb nslookup.  

![Alt]( images/dns03.png "Title")


Dins de la zona creada faràs un nou *Host (A)* amb el nom de Host *client* i la IP de la MV que fa de client.

(*) Mostra com l'has creat.

![Alt]( images/dns04.png "Title")


(*) Comprova que pots fer ping al nom del host *client.cognom.com* i que la traducció de nom a IP funciona amb nslookup.  

![Alt]( images/dns05.png "Title")


## 3.- Configuració de hosts amb IP pública:

Ara faràs un host nou que es dirà *google*, al qual li hauràs de donar una de les IPs reals de Google.com. 

Per a fer-ho ves a una màquina amb DNS configurat i busca una de les IPs de google.com.

(*) Comprova que a la màquina virtual client tradueix la direcció del host *google.cognom.com* amb nslookup i ping al nom de host.

![Alt]( images/dns06.png "Title") 

També faràs un host que es digui **amazon**, aquest host tindrà la IP real de amazon.com.

(*) Comprova que amb el navegador del client pots accedir a amazon.cognom.com i surt la web d'Amazon.

![Alt]( images/dns07.png "Title") 
     

## 4.- Servidors arrel:

Busca informació sobre els servidors arrel i respon les següents preguntes, a més possa un enllaça a la font d'informació que heu utilitzat:

**(*)** Què són els servidors arrel?

[Bàsicament, son el primer servidor DNS que es va a consultar (perquè és el que dóna el "punt inútil" que diu el Daniel). Apartir d'aquí aquest servidor redirigeix a altres servidors DNS que tenen més informació, com el següent domini.](https://ca.wikipedia.org/wiki/Servidor_arrel_de_noms)

**(*)** Quines són les IPs, noms i les empreses que gestionen els servidors arrel?

<table class="wikitable sortable">

<tbody>

<tr>

<th>Lletra</th>

<th>Adreça [IPv4](/wiki/IPv4 "IPv4")</th>

<th>Operador</th>

</tr>

<tr>

<th>[A](http://A.root-servers.org/)</th>

<td>198.41.0.4</td>

<td>[Verisign](/wiki/Verisign "Verisign")</td>

</tr>

<tr>

<th>[B](http://www.isi.edu/b-root/)</th>

<td>192.228.79.201<sup id="cite_ref-8" class="reference">[[note 3]](#cite_note-8)</sup><sup id="cite_ref-9" class="reference">[[6]](#cite_note-9)</sup></td>

<td>[USC](/wiki/University_of_Southern_California "University of Southern California")-[ISI](/w/index.php?title=Information_Sciences_Institute&action=edit&redlink=1 "Information Sciences Institute (encara no existeix)")</td>

</tr>

<tr>

<th>[C](http://c.root-servers.org/)</th>

<td>192.33.4.12</td>

<td>[Cogent Communications](/w/index.php?title=Cogent_Communications&action=edit&redlink=1 "Cogent Communications (encara no existeix)")</td>

</tr>

<tr>

<th>[D](http://d.root-servers.org/)</th>

<td>199.7.91.13<sup id="cite_ref-12" class="reference">[[note 4]](#cite_note-12)</sup><sup id="cite_ref-13" class="reference">[[9]](#cite_note-13)</sup></td>

<td>[University of Maryland](/w/index.php?title=University_of_Maryland,_College_Park&action=edit&redlink=1 "University of Maryland, College Park (encara no existeix)")</td>

</tr>

<tr>

<th>E</th>

<td>192.203.230.10</td>

<td>[NASA](/wiki/NASA "NASA")</td>

</tr>

<tr>

<th>[F](http://f.root-servers.org/)</th>

<td>192.5.5.241</td>

<td>[Internet Systems Consortium](/w/index.php?title=Internet_Systems_Consortium&action=edit&redlink=1 "Internet Systems Consortium (encara no existeix)")</td>

</tr>

<tr>

<th>[G](http://www.nic.mil/)</th>

<td>192.112.36.4</td>

<td>[Defense Information Systems Agency](/w/index.php?title=Defense_Information_Systems_Agency&action=edit&redlink=1 "Defense Information Systems Agency (encara no existeix)")</td>

</tr>

<tr>

<th>[H](http://h.root-servers.org/)</th>

<td>128.63.2.53</td>

<td>[U. S. Army Research Lab](/w/index.php?title=United_States_Army_Research_Laboratory&action=edit&redlink=1 "United States Army Research Laboratory (encara no existeix)")</td>

</tr>

<tr>

<th>[I](http://i.root-servers.org/)</th>

<td>192.36.148.17</td>

<td>[Netnod](/w/index.php?title=Netnod_Internet_Exchange_i_Sverige&action=edit&redlink=1 "Netnod Internet Exchange i Sverige (encara no existeix)")</td>

</tr>

<tr>

<th>[J](http://j.root-servers.org/)</th>

<td>192.58.128.30<sup id="cite_ref-22" class="reference">[[note 5]](#cite_note-22)</sup></td>

<td>[Verisign](/wiki/Verisign "Verisign")</td>

</tr>

<tr>

<th>[K](http://k.root-servers.org/)</th>

<td>193.0.14.129</td>

<td>[RIPE NCC](/wiki/RIPE_NCC "RIPE NCC")</td>

</tr>

<tr>

<th>[L](http://l.root-servers.org/)</th>

<td>199.7.83.42<sup id="cite_ref-27" class="reference">[[note 6]](#cite_note-27)</sup><sup id="cite_ref-28" class="reference">[[22]](#cite_note-28)</sup></td>

<td>[ICANN](/wiki/ICANN "ICANN")</td>

</tr>

<tr>

<th>[M](http://m.root-servers.org/)</th>

<td>202.12.27.33</td>

<td>[WIDE Project](/w/index.php?title=WIDE_Project&action=edit&redlink=1 "WIDE Project (encara no existeix)")</td>

</tr>

</tbody>

</table>

**(*)** Quins servidors arrel hi ha a les illes Hawai?

[Alohaweb](https://ioc.xtec.cat/materials/FP/Materials/2201_SMX/SMX_2201_M05/web/html/media/fp_smx_m05_material_paper.pdf) 
Les illes Hawaii, disposen de 3 servidors arrel:

![Alt]( images/dns21.png "Title") 
![Alt]( images/dns22.png "Title") 
![Alt]( images/dns23.png "Title") 

**(*)** Ara faràs una prova, executa nslookup *yahoo.fr* des del client de la teva xarxa NAT i mostra que no es pot resoldre el nom. 

![Alt]( images/dns08.png "Title") 

Afegiràs manualment els servidors arrel al servidor DNS, per això aniràs al servidor DNS -> Propiedades -> sugerencias de raíz i seleccionaràs **copiar desde el servidor** utilitzant 8.8.8.8 com a font.

El més probable es que no tinguis les IPs dels servidors, així que les introduïràs manualment ara que ja coneixes les seves IPs.

**(*)** Mostra les *sugerencias de raíz* un cop configurades.

![Alt]( images/dns09.png "Title") 

**(*)** Ara faràs una prova, executa nslookup *yahoo.fr* des del client de la teva xarxa NAT i mostra que si es pot resoldre el nom. 

![Alt]( images/dns10.png "Title") 

**(*)** Quan s'utilitzen els servidors arrel? Ordena les següents opcions des d'abans a després:

* Cache. (1)
* Zona. (2)
* Servidors arrel. (3)
* Reenviadors. (4)


## 5.- Reenviadors:

Ara treuràs els servidors arrel del servidor DNS.

Després configuraràs un reenviador, per això has d'anar a servidor DNS -> Propiedades -> Reenviadores i afegeix 8.8.8.8.

**(*)** Mostra la configuració del reenviador.

![Alt]( images/dns11.png "Title") 

Esborra la memòria cache del client i del servidor (el servidor Windows 2016 també té la seva memòria cache, la pots esborrar amb el botó dret sobre el nom del servidor i seleccionant "borrar caché").

**(*)** Prova a fer nslookup a yahoo.fr i alguns dominis més i comprova que es poden resoldre els noms gràcies al reenviador.

![Alt]( images/dns12.png "Title") 

**(*)** Què significa quan la reolució de noms ens diu **Respuesta no autorizativa**?

![Alt]( images/dns13.png "Title") 
    Significa que la resposta no l'ha trobat el nostre servidor DNS, si no fora gràcies al reenviador.
    

## 6.- Servidors DNS en xarxa (reenviadors):

Per aquesta part de la pràctica canviareu el mode de xarxa de les MV a **adaptador pont** d'aquesta manera podreu comunicar les MV entre vosaltres.

**(*)** La idea es que busqueu un parell de companys i intenteu fer una petició al vostre servidor DNS des del vostre client amb el nom "client.cognomCompany.com" i veieu que no es pot resoldre la petició.

![Alt]( images/dns14.png "Title") 
    
**(*)** Després creeu un reenviador des del vostre servidor al servidor del company (demaneu la seva IP) i proveu de resoldre la direcció "client.cognomCompany.com". Aquesta vegada sí hauria de funcionar.

![Alt]( images/dns15.png "Title") 

Feu això amb DOS COMPANYS de classe.

![Alt]( images/dns16.png "Title") 

