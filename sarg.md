#Instal·lar sarg per a Squid:

En aquesta pràctica has d'instal·lar sarg per a Squid. Tens un [tutorial aquí](https://dungeonofbits.com/instalacion-y-configuracion-de-sarg-para-squid.html).

1. Explica com has instal·lat sarg.
![Alt](images/sarg1.png)
Bàsicament, he descarregat el paquet del sarg, l'he instalat, l'he configurat dient-li on havia de mirar i què havia de mostrar i com. També he configurat l'apache2, perquè si accedim a "/var/www/html/squid-reports" localment, ens ho permeti. Com que he tingut problemes, he modificat l'arxiu .TTF tal com alerta el tutorial al final. 

Demana a algun company (o fes servir diferents equips) que es connecti a Internet mitjançant el teu proxy Squid.

2. Mostra una captura de access.log amb les connexions.
![Alt](images/sarg2.png)

3. Mostra una captura de sarg amb les mateixes connexions. 
![Alt](images/sarg3.png)
