En aquesta pràctica configuraràs ProFTPD per a treballar com un servidor FTPS, d'aquesta manera les connexions del canal de control i del canal de dades entre servidor i client aniran encriptades.

Fent servir [aquest tutorial](https://dungeonofbits.com/configuracion-del-servidor-ftp-proftpd-como-ftps.html) fes:

* Que un usuari pugui accedir per FTPS amb el client Filezilla des d'un equip diferent al servidor mostrant els passos seguits.
![Alt](images/ftps1.png)

* Que un usuari que es connecti al nostre servidor FTP sense FTPS *per terminal* també es pugui connectar, però no tingui informació del software ni la versió que fem servir de server FTP. Mostra els passos seguits.
![Alt](images/ftps2.png)

* Quan ho tinguis funcionant crida al mestre perquè es pugui connectar al teu server.

-El professor s'ha pogut connectar satisfactoriament.
